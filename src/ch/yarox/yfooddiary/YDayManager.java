package ch.yarox.yfooddiary;

import java.time.LocalDate;
import java.util.Scanner;

public class YDayManager {
	
	YDay day;
	Scanner scanner = new Scanner(System.in);
	
	public YDayManager(){
		day = readDay();
		if(day == null){
			day = new YDay();
			saveDay();
		}
	}
	
	public int printMenu(){
		System.out.println("-1) print day");
		System.out.println("0) end the program");
		System.out.println("1) Read day from file");
		System.out.println("2) Set breakfast");
		System.out.println("3) Set lunch ");
		System.out.println("4) Set dinner ");
		System.out.println("5) Set other ");
		System.out.println("Insert the number corresponding to the action :");
		int action = scanner.nextInt();
		scanner.nextLine();
		handleAction(action);
		saveDay();
		return action;
	}
	
	private void handleAction(int action){
		switch (action) {
		case -1:
			break;
		case 0:
			break;
		case 1:
			System.out.println("Insert date you want to load (format: dd-MM-yyyy)");
			day = YReadSaveDay.readDay(scanner.nextLine());
			break;
		case 2:
			System.out.println("Insert your breakfast");
			day.setBreakfast(scanner.nextLine());
			break;
		case 3:
			System.out.println("Insert your lunch");
			day.setLunch(scanner.nextLine());
			break;
		case 4:
			System.out.println("Insert your dinner");
			day.setDinner(scanner.nextLine());
			break;
		case 5:
			System.out.println("Insert your other thing \"time\"");
			String what = scanner.nextLine();
			System.out.println("Insert your other thing \"description\"");
			day.setOther(what, scanner.nextLine());
			break;
		default:
			System.out.println("Insert a number from 0 to 4!");
			break;
		}
		
	}
	
	public void saveDay(){
		YReadSaveDay.saveDay(day);
	}
	
	public YDay readDay(){
		return YReadSaveDay.readDay(LocalDate.now());
	}
	
	public void printDay(){
		System.out.println(day);
	}
}
