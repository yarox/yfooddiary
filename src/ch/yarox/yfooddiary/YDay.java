package ch.yarox.yfooddiary;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class YDay implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4904939976350711097L;
	
	private LocalDate date;
	private String breakfast;
	private String lunch;
	private String dinner;
	private Map<String, String> others;
	
	public YDay(){
		date = LocalDate.now();
		breakfast = "default colazione";
		lunch = "default pranzo";
		dinner = "default cena";
		others = new HashMap<String, String>();
	}
	
	public LocalDate getDate(){
		return date;
	}
	
	public void setBreakfast(String breakfast) {
		this.breakfast = breakfast;
	}

	public void setLunch(String lunch) {
		this.lunch = lunch;
	}

	public void setDinner(String dinner) {
		this.dinner = dinner;
	}

	public String toString(){
		String othersString = "";
		for(Entry<String,String> entry : others.entrySet()){
			othersString +=  "   " + entry.getKey() +  " : " + entry.getValue() + "\n";
		}
		return "Date: " + date + "\n breakfast: " + breakfast + "\n lunch: " + lunch + " \n dinner: " + dinner + "\n others: \n" + othersString;
	}
	
	public void setOther(String time, String description){
		others.put(time, description);
	}
}
