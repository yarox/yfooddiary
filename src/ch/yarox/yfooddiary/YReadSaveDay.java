package ch.yarox.yfooddiary;

import static ch.yarox.yfooddiary.util.Constants.FILE_EXTENSION;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class YReadSaveDay {
	public static void saveDay(YDay day) {
		try {
			FileOutputStream fout = new FileOutputStream("./" + day.getDate() + FILE_EXTENSION); 
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(day);
			oos.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static YDay readDay(LocalDate date) {
		try {
			FileInputStream fin = new FileInputStream("./" + date + FILE_EXTENSION);
			ObjectInputStream ois = new ObjectInputStream(fin);
			YDay day = (YDay) ois.readObject();
			ois.close();
			return day;
		} catch (Exception ex) {
			System.out.println("Error reading file!");
			return null;
		}
	}
	
	/**
	 * Return the objecet YDay from the file with the given date
	 * @param date with format dd-MM-yyyy
	 * @return YDay from file with passed date
	 */
	public static YDay readDay(String date){
		final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		//final String input = "23-06-2016";
		final LocalDate localDate = LocalDate.parse(date, DATE_FORMAT);
		return readDay(localDate);
	}
}
